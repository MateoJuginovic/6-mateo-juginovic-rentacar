window.onload = function () {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if ( this.readyState == 4 && this.status == 200 ) {
			$( '.js-cars-pull-request' ).html( this.responseText );
		}
	};

	xmlhttp.open( "GET", "php/vehicles-pull-request.php", true );
	xmlhttp.send();
}

$( window ).ready( function () {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if ( this.readyState == 4 && this.status == 200 ) {
			$( '.js-pagination-buttons' ).html( this.responseText );
		}

		$( '.js-card-list-pagination-item' ).first().addClass( 'card-list-pagination__item--active' );
	};

	xmlhttp.open( 'GET', 'php/pagination.php', true );
	xmlhttp.send();
} );

$( document ).on( 'click', '.js-card-list-pagination-item', function () {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if ( this.readyState == 4 && this.status == 200 ) {
			$( '.js-cars-pull-request' ).html( this.responseText );
		}
	};

	var pageNumber = $( this ).html();

	xmlhttp.open( "GET", "php/vehicles-pagination-request.php?page="+pageNumber, true );
	xmlhttp.send();

	$( '.js-vehicles-filter-city' ).val( '' );
	$( '.js-vehicles-filter-brand' ).val( '' );
	$( '.js-vehicles-filter-transmission' ).val( '' );
	$( '.js-vehicles-filter-color' ).val( '' );
	$( '.js-vehicles-filter-doors' ).val( '' );
	$( '.js-vehicles-filter-fuel' ).val( '' );
} );

$( document ).on( 'click', '.js-vehicles-filter-button', function ( e ) {
	e.preventDefault();

	$( '.js-card-list-pagination-item ' ).removeClass( 'card-list-pagination__item--active' );

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if ( this.readyState == 4 && this.status == 200 ) {
			$( '.js-cars-pull-request' ).html( this.responseText );
		}
	};

	let vehiclesFilterCity = $( '.js-vehicles-filter-city' ).find( ':selected' ).attr( 'value' );
	let vehiclesFilterBrand = $( '.js-vehicles-filter-brand' ).find( ':selected' ).attr( 'value' );
	let vehiclesFilterTransmission = $( '.js-vehicles-filter-transmission' ).find( ':selected' ).attr( 'value' );
	let vehiclesFilterColor = $( '.js-vehicles-filter-color' ).find( ':selected' ).attr( 'value' );
	let vehiclesFilterDoors = $( '.js-vehicles-filter-doors' ).find( ':selected' ).attr( 'value' );
	let vehiclesFilterFuel = $( '.js-vehicles-filter-fuel' ).find( ':selected' ).attr( 'value' );

	xmlhttp.open( "GET", "php/vehicles-filter-request.php?carCity="+vehiclesFilterCity+"&carBrand="+vehiclesFilterBrand+"&carTransmission="+vehiclesFilterTransmission+"&carColor="+vehiclesFilterColor+"&carDoors="+vehiclesFilterDoors+"&carFuel="+vehiclesFilterFuel, true );
	xmlhttp.send();
} );

$( document ).on( 'click', '.js-rental-submit', function () {
	var xmlhttp = new XMLHttpRequest();

	let carID = $( this ).attr( 'data-carID' );
	let customerName = $( '.js-popup-customerName' ).val();
	let customerSurname = $( '.js-popup-customerSurname' ).val();
	let customerEmail = $( '.js-popup-customerEmail' ).val();
	let customerContactNumber = $( '.js-popup-customerContactNumber' ).val();
	let pickUpDate = $( '.js-popup-pickUpDate' ).val();
	let returnDate = $( '.js-popup-returnDate' ).val();

	if ( customerName && customerSurname && customerEmail && customerContactNumber && pickUpDate && returnDate && carID ) {
		xmlhttp.open( "GET", "php/rentals.php?carID="+carID+"&customerName="+customerName+"&customerSurname="+customerSurname+"&customerEmail="+customerEmail+"&customerContactNumber="+customerContactNumber+"&pickUpDate="+pickUpDate+"&returnDate="+returnDate, true );
		xmlhttp.send();
	} else {
	}
} );
