<?php
	include 'connection.php';

	$result = mysqli_query( $con, "SELECT * FROM cars ORDER BY carPricePerDay LIMIT 0, 8" );

	while( $row = mysqli_fetch_array( $result ) ) {
		echo '<div class="col-12 col-6-sm col-4-md col-3-xl">
					<div class="card-list__item">
						<div class="card-list-item__image" style="background-image: url(images/content/'.$row["carImage"].'.jpg);"></div>

						<div class="card-list-item__main">
							<p class="text text--sm text--border text--mb-xs">'.$row["carYear"].'</p>

							<h5 class="title title--md title--mb-xs">'.$row["carBrand"]." ".$row["carModel"].'</h5>

							<p class="text text--md text--orange">$'.$row["carPricePerDay"]." ".'<span class="text text--md">$'.$row["carPricePerWeek"].'/week</span></p>
						</div>

						<ul class="card-list-item__footer">
							<div>
								<p class="text text--sm">'.$row["carDoors"].' doors</p>
							</div>

							<div>
								<p class="text text--sm">'.$row["carTransmission"].'</p>
							</div>

							<div>
								<p class="text text--sm">'.$row["carFuel"].'</p>
							</div>
						</ul>

						<a href="#rent-pop-up" class="button button--xl button--filled js-car-rent" data-carID="'.$row["carID"].'" data-carImage="'.$row["carImage"].'" data-carYear="'.$row["carYear"].'" data-carBrand="'.$row["carBrand"].'" data-carModel="'.$row["carModel"].'" data-carDoors="'.$row["carDoors"].'" data-carTransmission="'.$row["carTransmission"].'" data-carFuel="'.$row["carFuel"].'">Rent now</a>
					</div>
				</div>';
	}

	$con->close();
?>
